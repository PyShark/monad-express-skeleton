// flow-typed signature: cf9f6dde429fb508d4c2cb5464d74d4d
// flow-typed version: <<STUB>>/monet_v^0.8.10/flow_v0.65.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'monet'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'monet' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'monet/example/bower_components/jquery/jquery.min' {
  declare module.exports: any;
}

declare module 'monet/example/bower_components/underscore/underscore-min' {
  declare module.exports: any;
}

declare module 'monet/src/main/javascript/monet-pimp' {
  declare module.exports: any;
}

declare module 'monet/src/main/javascript/monet' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/curry_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/either_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/free_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/io_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/list_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/maybe_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/monad_laws' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/monad_transformer_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/nel_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/reader_spec' {
  declare module.exports: any;
}

declare module 'monet/src/test/javascript/validation_spec' {
  declare module.exports: any;
}

// Filename aliases
declare module 'monet/example/bower_components/jquery/jquery.min.js' {
  declare module.exports: $Exports<'monet/example/bower_components/jquery/jquery.min'>;
}
declare module 'monet/example/bower_components/underscore/underscore-min.js' {
  declare module.exports: $Exports<'monet/example/bower_components/underscore/underscore-min'>;
}
declare module 'monet/src/main/javascript/monet-pimp.js' {
  declare module.exports: $Exports<'monet/src/main/javascript/monet-pimp'>;
}
declare module 'monet/src/main/javascript/monet.js' {
  declare module.exports: $Exports<'monet/src/main/javascript/monet'>;
}
declare module 'monet/src/test/javascript/curry_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/curry_spec'>;
}
declare module 'monet/src/test/javascript/either_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/either_spec'>;
}
declare module 'monet/src/test/javascript/free_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/free_spec'>;
}
declare module 'monet/src/test/javascript/io_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/io_spec'>;
}
declare module 'monet/src/test/javascript/list_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/list_spec'>;
}
declare module 'monet/src/test/javascript/maybe_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/maybe_spec'>;
}
declare module 'monet/src/test/javascript/monad_laws.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/monad_laws'>;
}
declare module 'monet/src/test/javascript/monad_transformer_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/monad_transformer_spec'>;
}
declare module 'monet/src/test/javascript/nel_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/nel_spec'>;
}
declare module 'monet/src/test/javascript/reader_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/reader_spec'>;
}
declare module 'monet/src/test/javascript/validation_spec.js' {
  declare module.exports: $Exports<'monet/src/test/javascript/validation_spec'>;
}
