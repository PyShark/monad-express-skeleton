# Create API App

Simple backend scaffold for node APIs

# What's in it
- Mori -  Immutable Data Structures
- Monet - Monads
- Flow - Type System
- Jest - Test Runner & Assertion Library
- Sequelize - ORM
- Express - Routing
- ESLint - Linter
- PG - Postgres Driver
- Winston - General Logger
- Morgan - HTTP Middleware Logger

# Tasks
- syncdb - Run DB migration and seeding
- dev - Run syncdb and start live reload server
- build - Build project
- start - Start production server
- test - Run tests
- check - Run ESLint and Flow checker

