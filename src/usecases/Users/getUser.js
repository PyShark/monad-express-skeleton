// @flow

import {
  Either,
  type Maybe,
} from 'monet';
import errors from 'throw.js';
import type { UserRepository } from 'infrastructure/types/UserRepository';
import messages from 'infrastructure/messages';

export default async function getUser(
  repository: UserRepository,
  email: string,
): Promise<Either> {
  const result: Maybe = await repository.getUserByEmail(email);
  return result.toEither(new errors.NotFound(messages.user.notFound));
}
