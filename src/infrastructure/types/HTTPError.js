// @flow

export type HTTPError = {
  statusCode: number,
  message: string,
};
