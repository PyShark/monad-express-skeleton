// @flow

import type { Maybe } from 'monet';

export type RepositoryResponse = Promise<Maybe<any>>;
