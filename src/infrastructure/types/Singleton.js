// @flow

export interface Singleton<T> {
  getInstance(): T,
}
