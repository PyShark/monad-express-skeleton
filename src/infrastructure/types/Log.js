// @flow

export type Log = {
  level: string,
  message: string,
}
