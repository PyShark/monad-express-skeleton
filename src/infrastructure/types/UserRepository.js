// @flow

import type { Maybe } from 'monet';

export interface UserRepository {
  getUserByEmail(email: string): Promise<Maybe<{
    id: string,
    email: string,
    password: string
  }>>,
}
