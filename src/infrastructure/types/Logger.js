// @flow

import type { Log } from './Log';

export interface Logger {
  log(Log): void
}
