// @flow
import type {
  $Response,
  $Request,
} from 'express';
import errors from 'throw.js';
import { messages } from 'infrastructure';
import type { HTTPError } from 'infrastructure/types/HTTPError';
import { HTTPResponse } from './HTTPResponse';

export default class ResourceResponse implements HTTPResponse {
  success(
    res: $Response,
    message: string,
    status: number = 200,
    data?: Object,
  ) {
    res
      .status(status)
      .send({
        message,
        data,
      });
  }

  error(
    error: HTTPError,
    req: $Request,
    res: $Response,
    next: Function,
  ) {
    const status: number = error.statusCode || 500;
    res
      .status(status)
      .send({
        status,
        message: error.message || messages.server.serverError,
      });
  }

  unhandled(
    req: $Request,
    res: $Response,
    next: Function,
  ) {
    next(new errors.NotFound(messages.server.resourceNotFound));
  }
}
