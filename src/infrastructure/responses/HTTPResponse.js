// @flow

import type {
  $Response,
  $Request,
} from 'express';
import type { HTTPError } from 'infrastructure/types/HTTPError';

export interface HTTPResponse {
  success(
    res: $Response,
    message: string,
    status: number,
    data?: Object,
  ): void,

  error(
    error: HTTPError,
    req: $Request,
    res: $Response,
    next: Function,
  ): void,

  unhandled(
    req: $Request,
    res: $Response,
    next: Function,
  ): void,
}
