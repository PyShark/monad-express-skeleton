// @flow

export default {
  PROD: 'production',
  TEST: 'test',
  DEV: 'development',
};
