// @flow

import 'models';

export default function db<T>(callback: Function): T {
  return callback();
}
