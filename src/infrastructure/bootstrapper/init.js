// @flow

import type {
  $Request,
  $Response,
} from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import { spikeHandler } from 'infrastructure/handlers';
import { ENVIRONMENTS } from 'infrastructure/constants';

function logErrorsOnly(
  req: $Request,
  res: $Response,
): boolean {
  return res.statusCode < 400;
}

export default function init({ app, env, config }: Object): Object {
  app.enable('trust proxy');

  app.use(helmet(config.helmet));
  app.use(spikeHandler);

  if (env === ENVIRONMENTS.PROD) {
    app.use(morgan('combined', { skip: logErrorsOnly }));
  } else if (env === ENVIRONMENTS.DEV) {
    app.use(morgan('dev'));
    app.use(cors(config.cors));
  }

  app.use(bodyParser.json(config.parser));

  return app;
}
