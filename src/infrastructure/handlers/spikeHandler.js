// @flow

import toobusy from 'toobusy-js';
import errors from 'throw.js';
import type { $Response, $Request } from 'express';
import { messages } from 'infrastructure';

export default function spikeHandler(
  req: $Request,
  res: $Response,
  next: Function,
) {
  const maxLag: number = Number(process.env.APP_EVENT_QUEUE_LAG_THRESHOLD) || 70;
  toobusy.maxLag(Number(maxLag));

  next(toobusy() ? new errors.ServiceUnavailable(messages.server.serverTooBusy) : undefined);
}
