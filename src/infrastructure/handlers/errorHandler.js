// @flow

import type { HTTPResponse } from '../responses/HTTPResponse';

export default (
  app: Object,
  resourceResponse: HTTPResponse,
): Object => {
  app.use(resourceResponse.unhandled);
  app.use(resourceResponse.error);

  return app;
};
