// @flow

import {
  createLogger,
  transports,
  format,
} from 'winston';
import type { Singleton } from 'infrastructure/types/Singleton';
import type { Logger } from 'infrastructure/types/Logger';

const logPrintFormat: Function = format.printf((info: Object): string =>
  `${info.timestamp} ${info.level}: ${info.message}`);

const AppLogger: Singleton<Logger> = (function appLoggerConstructor(): Singleton<Logger> {
  let instance: Logger;

  function createInstance(): Logger {
    return createLogger({
      level: 'info',
      transports: [new transports.Console({
        format: format.combine(format.timestamp(), logPrintFormat),
      })],
    });
  }

  return {
    getInstance(): Logger {
      instance = !instance ? createInstance() : instance;
      return instance;
    },
  };
}());

export default AppLogger;
