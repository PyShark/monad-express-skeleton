// @flow

import server from './server';
import user from './user';

export default {
  server,
  user,
};
