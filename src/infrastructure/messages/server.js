// @flow

export default {
  resourceNotFound: 'The requested resource couldn\'t be found',
  serverTooBusy: 'This server is too busy right now. Try again later',
  serverError: 'There has been an error. Please try again.',
};
