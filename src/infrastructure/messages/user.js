// @flow

export default {
  notFound: 'User not found',
  found: 'User found',
};
