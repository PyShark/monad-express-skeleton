// @flow

import type {
  $Request,
  $Response,
  Middleware,
} from 'express';
import type { Either } from 'monet';
import { Users } from 'repository';
import { getUser } from 'usecases/Users';
import { ResourceResponse } from 'infrastructure/responses';
import messages from 'infrastructure/messages';
import type { HTTPError } from 'infrastructure/types/HTTPError';

function toFindUserDto(req: $Request): string {
  return req.query.email || '';
}

async function find(
  req: $Request,
  res: $Response,
  next: Function,
): Promise<Either> {
  try {
    const responder: ResourceResponse = new ResourceResponse();
    const findUserDto: string = toFindUserDto(req);
    const user: Either<HTTPError, Object> = await getUser(Users, findUserDto);

    user
      .cata(
        (err: HTTPError) => {
          responder.error(err, req, res, next);
        },
        (data: Object) => {
          responder.success(res, messages.user.found, 200, data);
        },
      );
  } catch (e) {
    next(e);
  }
}

const actions: { [string]: Middleware } = {
  find,
};

export default actions;
