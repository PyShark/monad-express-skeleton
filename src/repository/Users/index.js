// @flow

import { partial } from 'mori';
import db from 'models';
import { Maybe } from 'monet';
import getUserByEmail from './getUserByEmail';

const procedures: { [string]: (...any) => Maybe<any> } = {
  getUserByEmail: partial(getUserByEmail, db),
};

export default procedures;
