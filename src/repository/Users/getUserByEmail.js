// @flow

import type { Model } from 'sequelize';
import { Maybe } from 'monet';

function toPlainUser(user: Model<any>): Object {
  return user.get({ plain: true });
}

function toAllowedKeys(user: Object): Object {
  return {
    id: user.id,
    email: user.email,
    password: user.password,
  };
}

export default async function getUserByEmail(
  db: Object,
  email: string,
): Promise<Maybe> {
  const user: Model<any> = await db.user.findOne({
    where: {
      email,
    },
  });

  return Maybe
    .fromNull(user)
    .map(toPlainUser)
    .map(toAllowedKeys);
}
