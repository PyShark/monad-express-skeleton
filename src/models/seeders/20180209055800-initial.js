// @flow

import uuid from 'uuid/v4';
import type { QueryInterface } from 'sequelize';

const ADMIN_EMAIL: string = 'admin@stationfive.com';

export default {
  up(queryInterface: QueryInterface): Promise {
    return queryInterface.bulkInsert('user', [
      {
        id: uuid(),
        email: ADMIN_EMAIL,
        password: 'abc123',
      },
    ]);
  },

  down(queryInterface: QueryInterface): Promise {
    return queryInterface.bulkDelete('user', {
      email: ADMIN_EMAIL,
    });
  },
};
