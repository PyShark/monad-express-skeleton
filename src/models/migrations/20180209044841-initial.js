// @flow

import type { QueryInterface } from 'sequelize';

export default {
  up(
    queryInterface: QueryInterface,
    DataTypes: Object,
  ): Promise {
    return queryInterface.createTable('user', {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE,
    });
  },

  down(queryInterface: QueryInterface): Promise {
    return queryInterface.dropTable('user');
  },
};
