// @flow

import type {
  Sequelize,
  Model,
} from 'sequelize';

export default function User(
  sequelize: Sequelize,
  DataTypes: Object,
): Model {
  return sequelize.define('user', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    freezeTableName: true,
    underscored: true,
  });
}
