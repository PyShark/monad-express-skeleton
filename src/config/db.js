// @flow

import dotenv from 'dotenv';
import AppLogger from 'infrastructure/logger';
import type { Logger } from 'infrastructure/types/Logger';

dotenv.config({ silent: true });

const instance: Logger = AppLogger.getInstance();

export default {
  uri: process.env.SQL_URI,
  dialect: 'postgres',
  options: {
    pool: {
      max: process.env.SQL_MAX_CONN || 20,
      min: process.env.SQL_MIN_CONN || 5,
      idle: process.env.SQL_IDLE || 10000,
    },
    dialectOptions: process.env.SQL_OPTIONS ? process.env.SQL_OPTIONS : {},
    logging: +process.env.SQL_LOGGING ? instance.log : false,
  },
};
