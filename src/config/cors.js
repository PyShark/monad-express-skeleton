// @flow

export default {
  exposedHeaders: ['Link'],
  origin: '*',
};
