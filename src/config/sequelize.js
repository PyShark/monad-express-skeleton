require('babel-register');
require('babel-polyfill');

const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  development: {
    url: process.env.SQL_URI,
    dialect: 'postgres',
    seederStorage: 'sequelize',
    seederStorageTableName: 'seeds',
    migrationStorageTableName: 'migrations',
  },
};
