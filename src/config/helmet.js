// @flow

export default {
  contentSecurityPolicy: {
    directives: {
      defaultSrc: ['\'self\''],
    },
  },
  noCache: true,
  frameguard: true,
  noSniff: true,
  referrerPolicy: {
    policy: 'no-referrer-when-downgrade',
  },
  xssFilter: true,
};
