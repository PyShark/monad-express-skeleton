// @flow

import cors from './cors';
import db from './db';
import helmet from './helmet';
import parser from './parser';
import sequelize from './sequelize';

export default {
  cors,
  db,
  helmet,
  parser,
  sequelize,
};
