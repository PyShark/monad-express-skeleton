// @flow

import express from 'express';
import api from 'api';
import { ResourceResponse } from 'infrastructure/responses';
import type { HTTPResponse } from 'infrastructure/responses';
import { errorHandler } from 'infrastructure/handlers';
import { bootstrapper } from 'infrastructure';
import config from 'config';

export default function apiApp(): Object {
  const app: Object = express();
  const env: Object = app.get('env');
  const resourceResponse: HTTPResponse = new ResourceResponse();

  bootstrapper.init({
    app,
    env,
    config,
  });

  return bootstrapper.db((): Object => {
    app.use('/v1', api);
    errorHandler(app, resourceResponse);
    return app;
  });
}
