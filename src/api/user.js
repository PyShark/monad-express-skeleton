// @flow

import { Router } from 'express';
import { UserController } from 'controllers';

const user: Router = Router();

user.route('/find')
  .get(UserController.find);

export default user;
