// @flow

import { Router } from 'express';
import user from './user';

const api: Object = Router();

api.use('/user', user);

export default api;
