// @flow

import http from 'http';
import dotenv from 'dotenv';
import AppLogger from 'infrastructure/logger';
import type { Logger } from 'infrastructure/types/Logger';
import apiApp from './app';

dotenv.config({ silent: true });

const app: Object = apiApp();

app.set('port', process.env.PORT || 3000);

const server: Object = http.createServer(app);

server.on('error', (error: Error) => {
  throw error;
});

server.on('listening', () => {
  const addr: Object = server.address();
  const logger: Logger = AppLogger.getInstance();
  logger.log({
    level: 'info',
    message: `Listening on port ${addr.port}`,
  });
});

server.listen(app.get('port'));
